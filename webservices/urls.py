from django.urls import include, path
from rest_framework import routers, serializers, viewsets
from . import views

from rest_framework_simplejwt.views import (
    TokenRefreshView,
)

webservices_patterns = [
    path('login/', views.TokenObtainPairView.as_view()),
    path('token/refresh/', TokenRefreshView.as_view()),
]
