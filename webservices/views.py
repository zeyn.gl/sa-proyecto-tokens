from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from rest_framework_simplejwt.views import TokenObtainPairView
from rest_framework import permissions, status
from rest_framework.response import Response
from rest_framework.views import APIView
from scope.models import Asignation
from rest_framework_simplejwt import authentication
import requests, json

allowed_chars = 'abcdefghjkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ23456789'

class TokenObtainPairSerializer(TokenObtainPairSerializer):
    """
    Metodo que se utiliza para obtener token pasando como parametros el id y password y devolviendo el token serializado
    """

    def validate(self, attrs):
        data = super().validate(attrs)
        refresh = self.get_token(self.user)
        data['refresh'] = str(refresh)
        data['token'] = str(refresh.access_token)
        data.pop('access')
        return data

    @classmethod
    def get_token(cls, user):
        token = super(TokenObtainPairSerializer, cls).get_token(user)
        scope = Asignation.objects.filter(user=user).values_list('scope__name', flat=True)
        final_scope = []
        for attribute in scope:
            final_scope.append(attribute)
        token['scope'] = final_scope

        return token


class TokenObtainPairView(TokenObtainPairView):
    """
    Vista que para obtener el token de autorizacion
    """
    serializer_class = TokenObtainPairSerializer


class getToken(APIView):

    def post(self, request):
        if 'client_id' in request.data and 'client_secret' in request.data:
            client_id = request.data['client_id']
            client_secret = request.data['client_secret']
            params = {'username': client_id,
                      'password': client_secret}

            #CONSULTA CON SERVICIO DE ASEGURADORA
            ws_url = 'http://127.0.0.1:8000/login/'
            response = requests.post(ws_url, json=params)

        return Response(json.loads(response.text), status=response.status_code)




