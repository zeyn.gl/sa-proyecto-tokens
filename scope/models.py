from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _

# Create your models here.

class ScopeManager(models.Manager):
    """
    Manejador de alcance que permite el control en creacion, edicion y obtencion
    """

    def get_scopes(self):
        return self.get_queryset()


class Scope(models.Model):
    """
    Modelo para alcances
    """
    objects = ScopeManager()

    name = models.CharField(_('Nombre'), max_length=75)

    def __str__(self):
        return self.name


class AsignationManager(models.Manager):
    """
    Manejador de asignacion de alcance que permite el control en creacion, edicion y obtencion
    """

    def get_scopes(self):
        return self.get_queryset()


class Asignation(models.Model):
    """
    Modelo para asignacion de alcance
    """
    objects = ScopeManager()


    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE,
        null=False,
        verbose_name=_('Usuario')
    )

    scope = models.ManyToManyField(
        Scope,
        verbose_name=_('Alcance'),
    )


    def __str__(self):
        return self.user.username