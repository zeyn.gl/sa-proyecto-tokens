from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as DjangoUserAdmin
from django.utils.translation import ugettext_lazy as _

from .models import Scope, Asignation


@admin.register(Scope)
class Scope(admin.ModelAdmin):
    fieldsets = [
        (_('Detalles'), {
            'classes': ('wide',),
            'fields': [
                'name',
            ]
        }),
    ]

    add_fieldsets =(
        (_('Detalles'), {
            'classes': ('wide',),
            'fields': [
                'name',
            ]
        }),
    )
    list_filter = ('name', )
    list_display = ('name', )
    ordering = ('name',)

@admin.register(Asignation)
class Asignation(admin.ModelAdmin):
    fieldsets = [
        (_('Detalles'), {
            'classes': ('wide',),
            'fields': [
                'user',
                'scope'
            ]
        }),
    ]

    add_fieldsets =(
        (_('Detalles'), {
            'classes': ('wide',),
            'fields': [
                'user',
                'scope'
            ]
        }),
    )
    list_filter = ('user', )
    list_display = ('user', )
    ordering = ('user',)
